use std::cmp::Ordering;

/// Graham's Scan
/// Page 48.
/// Best, Average and Worst: `O(n*logn)`.
fn graham(points: &mut [Point]) -> Vec<Point> {
    dbg!(&points);
    let n = points.len();
    let mut point0 = points[0];
    let mut point0_position = 0;
    for i in 1..n {
        let point = points[i];
        if point.y < point0.y || (point.y == point0.y && point.x < point0.x) {
            point0 = point;
            point0_position = i;
        }
    }
    dbg!(&point0);
    points.swap(point0_position, 0);
    dbg!(&points);
    points[1..].sort_by(|point1, point2| Point::compare(&point0, point1, point2));
    dbg!(&points);
    let mut hull = vec![points[0], points[1]];
    for i in 2..n {
        while let Direction::Clockwise = Direction::calculate(
            hull.get(hull.len() - 2).unwrap(),
            hull.last().unwrap(),
            &points[i],
        ) {
            hull.pop();
        }
        hull.push(points[i]);
    }
    hull
}

#[derive(Debug, Copy, Clone, PartialEq)]
struct Point {
    x: f64,
    y: f64,
}

impl Point {
    fn into_tuple(self) -> (f64, f64) {
        (self.x, self.y)
    }

    fn into_f32tuple(self) -> (f32, f32) {
        (self.x as f32, self.y as f32)
    }

    /// Compares `point1` and `point2` based on ascending [`Direction::AntiClockwise`]
    /// order.
    fn compare(point0: &Point, point1: &Point, point2: &Point) -> Ordering {
        match Direction::calculate(point0, point1, point2) {
            Direction::AntiClockwise => Ordering::Less,
            Direction::Clockwise => Ordering::Greater,
            Direction::Collinear => Ordering::Equal,
        }
    }
}

impl<T> From<(T, T)> for Point
where
    T: Into<f64>,
{
    fn from((x, y): (T, T)) -> Self {
        Point {
            x: x.into(),
            y: y.into(),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Direction {
    Clockwise,
    AntiClockwise,
    Collinear,
}

impl Direction {
    /// Calculate in which direction should we rotate or vector `[point0, point1]` to intersect
    /// `point2`.
    fn calculate(point0: &Point, point1: &Point, point2: &Point) -> Direction {
        match ((point1.x - point0.x) * (point2.y - point1.y)
            - (point1.y - point0.y) * (point2.x - point1.x))
            .partial_cmp(&0.0)
            .unwrap()
        {
            Ordering::Greater => Direction::AntiClockwise,
            Ordering::Less => Direction::Clockwise,
            Ordering::Equal => Direction::Collinear,
        }
    }
}

#[cfg(test)]
mod tests {
    use plotters::prelude::*;

    use crate::*;

    #[test]
    fn calculate_direction() {
        let point0 = (0, 0).into();
        let point1 = (1, 0).into();
        let point2 = (1, 1).into();
        assert_eq!(
            Direction::calculate(&point0, &point1, &point2),
            Direction::AntiClockwise
        );
        assert_eq!(
            Direction::calculate(&point0, &point2, &point1),
            Direction::Clockwise
        );
        let point3 = (2, 2).into();
        assert_eq!(
            Direction::calculate(&point0, &point2, &point3),
            Direction::Collinear
        );
    }

    #[test]
    fn compare() {
        let point0 = (0, 0).into();
        let point1 = (0, 2).into();
        let point2 = (2, 2).into();
        assert_eq!(Point::compare(&point0, &point1, &point2), Ordering::Greater);
        assert_eq!(Point::compare(&point0, &point2, &point1), Ordering::Less);
        let point3 = (3, 3).into();
        assert_eq!(Point::compare(&point0, &point2, &point3), Ordering::Equal);
        let point4 = (2, 0).into();
        assert_eq!(Point::compare(&point0, &point2, &point4), Ordering::Greater);
        assert_eq!(Point::compare(&point0, &point1, &point4), Ordering::Greater);
    }

    #[test]
    fn graham() {
        let mut points = [(2, 0), (0, 2), (0, 0), (2, 2)].map(Point::from);
        let root = BitMapBackend::new("0.png", (640, 480)).into_drawing_area();
        root.fill(&WHITE).unwrap();
        let mut chart = ChartBuilder::on(&root)
            .caption("hull", ("sans-serif", 50).into_font())
            .margin(5u32)
            .x_label_area_size(30u32)
            .y_label_area_size(30u32)
            .build_cartesian_2d(-10f32..10f32, -10f32..10f32)
            .unwrap();

        chart.configure_mesh().draw().unwrap();

        chart
            .draw_series(LineSeries::new(
                points.iter().cloned().map(Point::into_f32tuple),
                &RED,
            ))
            .unwrap()
            .label("points");
        let result = super::graham(&mut points);
        chart
            .draw_series(LineSeries::new(
                result.iter().cloned().map(Point::into_f32tuple),
                &GREEN,
            ))
            .unwrap()
            .label("hull");

        chart
            .configure_series_labels()
            .background_style(&WHITE.mix(0.8))
            .border_style(&BLACK)
            .draw()
            .unwrap();
        assert_eq!(result, [(0, 0), (2, 0), (2, 2), (0, 2)].map(Point::from));
    }

    #[test]
    fn graham_big() {
        let mut points = [
            (-7, 8),
            (-4, 6),
            (2, 6),
            (6, 4),
            (8, 6),
            (7, -2),
            (4, -6),
            (8, -7),
            (0, 0),
            (3, -2),
            (6, -10),
            (0, -6),
            (-9, -5),
            (-8, -2),
            (-8, 0),
            (-10, 3),
            (-2, 2),
            (-10, 4),
        ]
        .map(Point::from);
        let root = BitMapBackend::new("1.png", (640, 480)).into_drawing_area();
        root.fill(&WHITE).unwrap();
        let mut chart = ChartBuilder::on(&root)
            .caption("hull", ("sans-serif", 50).into_font())
            .margin(5u32)
            .x_label_area_size(30u32)
            .y_label_area_size(30u32)
            .build_cartesian_2d(-10f32..10f32, -10f32..10f32)
            .unwrap();

        chart.configure_mesh().draw().unwrap();

        chart
            .draw_series(LineSeries::new(
                points.iter().cloned().map(Point::into_f32tuple),
                &RED,
            ))
            .unwrap()
            .label("points");
        let result = super::graham(&mut points);
        chart
            .draw_series(LineSeries::new(
                result.iter().cloned().map(Point::into_f32tuple),
                &GREEN,
            ))
            .unwrap()
            .label("hull");

        chart
            .configure_series_labels()
            .background_style(&WHITE.mix(0.8))
            .border_style(&BLACK)
            .draw()
            .unwrap();
        assert_eq!(
            result,
            [
                (6, -10),
                (8, -7),
                (8, 6),
                (-7, 8),
                (-10, 4),
                (-10, 3),
                (-9, -5),
            ]
            .map(Point::from)
        );
    }
}
