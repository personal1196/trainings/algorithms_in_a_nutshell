# algorithms_in_a_nutshell

Walk-through [Algorithms in a Nutshell](https://www.oreilly.com/library/view/algorithms-in-a/9780596516246/) book and implementation of described algorithms in different languages.

## Current list of languages

* Rust
* Racket

## Interesting languages to use

* Elm
* Dart
